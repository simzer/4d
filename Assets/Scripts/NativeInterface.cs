﻿using System.Runtime.InteropServices;

class NativeInterface {

	[DllImport("4DApp")]
	public static extern void resetCamera();

	[DllImport("4DApp")]
	public static extern void moveCamera(float rotXY, float rotYZ, float rotXZ,
					float rotXW, float rotYW, float rotZW,
	                float movX, float movY, float movZ, float movW);

	[DllImport("4DApp")]
	public static extern float cameraPos(int axis);

	[DllImport("4DApp")]
	public static extern float cameraDirection(int axis);
	
	[DllImport("4DApp")]
	public static extern float cameraUp(int axis);

	[DllImport("4DApp")]
	public static extern void resetIterators();

	[DllImport("4DApp")]
	public static extern bool hasObject();

	[DllImport("4DApp")]
	public static extern bool hasFacet();

	[DllImport("4DApp")]
	public static extern bool hasVertex();

	[DllImport("4DApp")]
	public static extern void nextObject();

	[DllImport("4DApp")]
	public static extern void nextFacet();

	[DllImport("4DApp")]
	public static extern void nextVertex();

	[DllImport("4DApp")]
	public static extern float vertexCoord(int axis);

	[DllImport("4DApp")]
	public static extern float vertexMetadata();

	[DllImport("4DApp")]
	public static extern void enableLog(bool enable);

	[DllImport("4DApp")]
	public static extern void triggerLog();

}

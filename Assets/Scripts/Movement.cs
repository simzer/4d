﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

	// order: XZ, YZ, XW, movement
	private Vector4 rotation;
	private Vector4 movement;
	private bool logEnabled;

	private void Start () {
		rotation = new Vector4();
		movement = new Vector4();
		logEnabled = false;
		Screen.lockCursor = true;
	}
	
	private void Update () {
		rotation = Vector4.zero;
		movement = Vector4.zero;
		rotation.x = Input.GetAxis("Mouse X");
		rotation.y = -Input.GetAxis("Mouse Y");

		if (Input.GetKey(KeyCode.H)) {
			rotation.x += -0.1f;
		}
		if (Input.GetKey(KeyCode.K)) {
			rotation.x += 0.1f;
		}
		
		if (Input.GetKey(KeyCode.U)) {
			rotation.y += -0.1f;
		}
		if (Input.GetKey(KeyCode.J)) {
			rotation.y += 0.1f;
		}

		if (Input.GetKey(KeyCode.Q)) {
			rotation.z = -0.1f;
		}
		if (Input.GetKey(KeyCode.E)) {
			rotation.z = 0.1f;
		}

		if (Input.GetKey(KeyCode.W)) {
			movement.x = 0.1f;
		}
		if (Input.GetKey(KeyCode.S)) {
			movement.x = -0.1f;
		}
		
		if (Input.GetKey(KeyCode.A)) {
			movement.z = 0.1f;
		}
		if (Input.GetKey(KeyCode.D)) {
			movement.z = -0.1f;
		}

		if (Input.GetKey(KeyCode.L)) {
			logEnabled = !logEnabled;
		}
		rotation = rotation / 5;
	}

	public Vector4 GetRotation() {
		return rotation;
	}

	public Vector4 GetMovement() {
		return movement;
	}


	public bool GetLogEnabled() {
		return logEnabled;
	}
}
